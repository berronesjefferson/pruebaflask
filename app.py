from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from apps.tipti.containers import Container
from core.config.config import config
from core.config.db import db

# Routes
from apps.rrhh.routes import User
from apps.tipti.ports.routers.student_router import student_blueprint

app = Flask(__name__)

CORS(app, resource={'*': {'origins': "http://localhost:9300"}})

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///db.sqlite"
migrate = Migrate(app, db)
SQLAlchemy(app)


# SWAGGER
# Swagger(app, config=swagger_config, template=template)


@app.before_request
def before_request():
    # print("antes de la peticion")
    pass


@app.after_request
def after_request(response):
    # print(f"despues de la peticion")
    return response


def page_no_found(error):
    return '<h1>Page 404</h1>'


if __name__ == '__main__':
    container = Container()
    app.container = container
    # Config
    # app.config.from_object(config)
    app.config.from_object(config['development'])
    app.config.from_mapping()
    # configure_inject(application)

    # Blueprints
    app.register_blueprint(User.main, url_prefix='/api/v1')
    # app.register_blueprint(StudentRouter.main, url_prefix='/api/v1')
    app.register_blueprint(student_blueprint(), url_prefix='/api/v1')

    # Error handlers
    app.register_error_handler(404, page_no_found)
    app.run()

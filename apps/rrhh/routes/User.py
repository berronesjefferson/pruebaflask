from flask import Blueprint, jsonify, request

from apps.rrhh.models.UserModel import UserModel
from core.constants.http_status_codes import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_200_OK, HTTP_404_NOT_FOUND
from ..models.models import User

main = Blueprint('user', __name__)


@main.route('/users')
def get_users():
    try:
        users = UserModel.get_users()
        return jsonify([x for x in users]), HTTP_200_OK
    except Exception as ex:
        return jsonify({'message': str(ex)}), HTTP_500_INTERNAL_SERVER_ERROR


@main.route('/users/<id>')
def get_user(id):
    try:
        user_or_none = UserModel.get_user(id)
        if user_or_none is not None:
            return jsonify(user_or_none), HTTP_200_OK
        return jsonify({}), HTTP_404_NOT_FOUND
    except Exception as ex:
        return jsonify({'message': str(ex)}), HTTP_500_INTERNAL_SERVER_ERROR


# VALIDAR QUE LOS PARAMETROS LLEGUEN DE FORMA CORRECTA
@main.route('/users/add', methods=["POST"])
def add_user():
    try:
        user = User.from_JSON(request.json)
        UserModel.add_user(user)
        # TODO: OBTENER EL ULTIMO REGISTRO INSERTADO Y DEVOLVER ID
        return jsonify({"resp": True}), HTTP_200_OK
    except Exception as ex:
        return jsonify({'message': str(ex)}), HTTP_500_INTERNAL_SERVER_ERROR


# VALIDAR QUE LOS PARAMETROS LLEGUEN DE FORMA CORRECTA
@main.route('/users/update/<id>', methods=["PUT"])
def update_user(id):
    try:
        user = User.from_JSON(request.json)
        user.id = id
        UserModel.update_user(user)
        # TODO: OBTENER EL ULTIMO REGISTRO INSERTADO Y DEVOLVER ID
        return jsonify({"resp": True}), HTTP_200_OK
    except Exception as ex:
        return jsonify({'message': str(ex)}), HTTP_500_INTERNAL_SERVER_ERROR


@main.route('/users/delete/<id>', methods=["DELETE"])
def delete_user(id):
    try:
        UserModel.delete_user(id)
        # TODO: LOGICA DE VALIDACION CUANDO NO EXISTE UN REGISTRO
        return jsonify({"resp": True}), HTTP_200_OK
    except Exception as ex:
        return jsonify({'message': str(ex)}), HTTP_500_INTERNAL_SERVER_ERROR

from core.config.db import db, BaseModelMixin
from typing import NamedTuple, Optional


# https://flask-sqlalchemy.palletsprojects.com/en/2.x/config/#connection-uri-format
# postgresql://scott:tiger@localhost/mydatabase
# MySQL:
# mysql://scott:tiger@localhost/mydatabase
# Oracle:
# oracle://scott:tiger@127.0.0.1:1521/sidname

# flask db init -> ejecutar como comando
# flask db migrate -> ejecutar como comando
# flask db upgrade -> ejecutar como comando


# from app import *
# db.create_all()
# sqlite3 name_database


# db.session.add(User(name="Flask", email="example@example.com"))
# db.session.commit()

# users = User.query.all()


class User(db.Model, BaseModelMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String, unique=True, nullable=False)
    email = db.Column(db.String, unique=True, nullable=False)

    def __repr__(self):
        return f'<User {self.id} {self.username} - {self.email}>'

    def to_JSON(self):
        return {
            'id': self.id,
            'username': self.username,
            'email': self.email,
        }

    @classmethod
    def from_JSON(self, json_parameter):
        return User(
            username=json_parameter['username'],
            email=json_parameter['email'],
        )

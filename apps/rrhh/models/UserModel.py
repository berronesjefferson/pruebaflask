from core.config.db import db
from ..models.models import User


class UserModel:

    @classmethod
    def get_users(self):
        try:
            # return [x.to_JSON() for x in User.query.all()]
            return [x.to_JSON() for x in User.get_all()]
        except Exception as ex:
            raise Exception(ex)

    @classmethod
    def get_user(self, id):
        try:
            # user_or_none = User.query.get(id)
            user_or_none = User.get_by_id(id)
            if user_or_none is not None:
                return user_or_none.to_JSON()
            return None
        except Exception as ex:
            raise Exception(ex)

    @classmethod
    def add_user(self, user):
        try:
            # with db.session.begin():
            user.save()
            print("ultimo registro insertado", user.id)
            # db.session.add(user)
            # db.session.commit()
            # user_new = User(
            #     username=None,
            #     email=1,
            # )
            # db.session.add(user_new)
            db.session.commit()
            return user
        except Exception as ex:
            # db.session.rollback()
            raise Exception(ex)

    @classmethod
    def update_user(self, user_parameter):
        try:
            user = User.query.get(user_parameter.id)
            user.username = user_parameter.username
            user.email = user_parameter.email
            user.commit()
            # db.session.commit()
            return user
        except Exception as ex:
            raise Exception(ex)

    @classmethod
    def delete_user(self, id):
        try:
            user = User.query.get(id)
            # db.session.delete(user)
            # db.session.commit()
            user.delete()
        except Exception as ex:
            raise Exception(ex)

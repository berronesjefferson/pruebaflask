from flask import Blueprint, jsonify, request
from dependency_injector.wiring import inject, Provide
from apps.tipti.containers import Container

from apps.tipti.domain.student.student_manager import StudentManager
from apps.tipti.domain.model.student import Student
from core.constants.http_status_codes import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_200_OK, HTTP_404_NOT_FOUND


@inject
def student_blueprint(student_manager: StudentManager = Provide[Container.student_manager_impl]) -> Blueprint:
    main = Blueprint('student', __name__)

    @main.route('/students')
    def get_students():
        try:
            return jsonify([x for x in student_manager.list()]), HTTP_200_OK
        except Exception as ex:
            return jsonify({'message': str(ex)}), HTTP_500_INTERNAL_SERVER_ERROR

    @main.route('/students/<id>')
    def get_student(id):
        try:
            student_or_none = student_manager.get_student(id)
            if student_or_none is not None:
                return jsonify(student_or_none), HTTP_200_OK
            return jsonify({}), HTTP_404_NOT_FOUND
        except Exception as ex:
            return jsonify({'message': str(ex)}), HTTP_500_INTERNAL_SERVER_ERROR

    # Todo: VALIDAR QUE LOS PARAMETROS LLEGUEN DE FORMA CORRECTA
    @main.route('/students/add', methods=["POST"])
    def add_student():
        try:
            student_manager.create(student=Student.from_json(request_json=request.json))
            return jsonify({"resp": True}), HTTP_200_OK
        except Exception as ex:
            return jsonify({'message': str(ex)}), HTTP_500_INTERNAL_SERVER_ERROR

    # Todo: VALIDAR QUE LOS PARAMETROS LLEGUEN DE FORMA CORRECTA
    @main.route('/students/update/<id>', methods=["PUT"])
    def update_student(id):
        try:
            student_manager.update(student=Student.from_json(request_json=request.json))
            return jsonify({"resp": True}), HTTP_200_OK
        except Exception as ex:
            return jsonify({'message': str(ex)}), HTTP_500_INTERNAL_SERVER_ERROR

    @main.route('/students/delete/<id>', methods=["DELETE"])
    def delete_student(id):
        try:
            student_manager.delete(id)
            return jsonify({"resp": True}), HTTP_200_OK
        except Exception as ex:
            return jsonify({'message': str(ex)}), HTTP_500_INTERNAL_SERVER_ERROR

    return main

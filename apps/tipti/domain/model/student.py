from dataclasses import dataclass

from apps.tipti.infrasctructure.database.entity.student_entity import StudentEntity


@dataclass
class Student:
    first_name: str = ""
    last_name: str = ""
    email: str = ""
    age: int = 0

    # __first_name: str
    # __last_name: str
    # __email: str
    # __age: int

    # def __init__(self, first_name, last_name, email, age):
    #     self.__first_name = first_name
    #     self.__last_name = last_name
    #     self.__email = email
    #     self.__age = age

    # def __init__(self, first_name, last_name, email, age):
    #     self.first_name = first_name
    #     self.last_name = last_name
    #     self.email = email
    #     self.age = age

    @staticmethod
    def from_json(request_json):
        return Student(
            first_name=request_json['first_name'],
            last_name=request_json['last_name'],
            email=request_json['email'],
            age=request_json['age'],
        )

    def to_database(self):
        return StudentEntity(
            first_name=self.first_name,
            last_name=self.last_name,
            email=self.email,
            age=self.age,
        )

from abc import (
    ABCMeta,
    abstractmethod
)

from apps.tipti.domain.model.student import Student


class StudentManager:
    __metaclass__ = ABCMeta

    @abstractmethod
    def list(self) -> [Student]:
        pass

    @abstractmethod
    def get_student(self, id) -> Student:
        pass

    @abstractmethod
    def create(self, student: Student) -> None:
        pass

    @abstractmethod
    def update(self, student: Student) -> None:
        pass

    @abstractmethod
    def delete(self, id) -> None:
        pass

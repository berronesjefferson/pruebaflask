from apps.tipti.domain.student.student_manager import StudentManager
from apps.tipti.domain.model.student import Student
from apps.tipti.infrasctructure.repository.student_repository import StudentRepository


class StudentManagerImpl(StudentManager):

    def __init__(self, repository: StudentRepository):
        self.repository = repository

    def list(self):
        student_list = self.repository.get_list()
        if not student_list:
            student_list = self.repository.get_list_from_api()
            [self.repository.create(student_entity=student.to_database()) for student in student_list]
        return student_list

    def get_student(self, id) -> Student:
        return self.repository.get_student(id)

    def create(self, student: Student) -> None:
        # self.repository.create(student_entity=student.to_database())
        pass

    def update(self, student: Student) -> None:
        # self.repository.update(student_entity=student.to_database())
        pass

    def delete(self, id) -> None:
        self.repository.delete(id)

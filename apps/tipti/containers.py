"""Containers module."""

from dependency_injector import containers, providers

from .domain.manager.student_manager_impl import StudentManagerImpl
from .infrasctructure.network.student_service import StudentService
from apps.tipti.infrasctructure.repository.student_repository_impl_db import StudentRepositoryImplDb


class Container(containers.DeclarativeContainer):
    wiring_config = containers.WiringConfiguration(modules=[".ports.routers.student_router"])

    student_service = providers.Factory(
        StudentService
    )

    student_repository_impl_db = providers.Factory(
        StudentRepositoryImplDb,
        student_service=student_service,
    )

    student_manager_impl = providers.Factory(
        StudentManagerImpl,
        repository=student_repository_impl_db,
    )

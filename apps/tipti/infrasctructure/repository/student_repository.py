from abc import (
    ABCMeta,
    abstractmethod
)

from apps.tipti.domain.model.student import Student
from apps.tipti.infrasctructure.database.entity.student_entity import StudentEntity


class StudentRepository(metaclass=ABCMeta):
    # class StudentRepository:
    #     __metaclass__ = ABCMeta

    @abstractmethod
    def get_list(self) -> [Student]:
        pass

    @abstractmethod
    def get_student(self, id) -> Student:
        pass

    @abstractmethod
    def create(self, student_entity: StudentEntity) -> None:
        pass

    @abstractmethod
    def update(self, student_entity: StudentEntity) -> None:
        pass

    @abstractmethod
    def delete(self, id) -> None:
        pass

    @abstractmethod
    def get_list_from_api(self) -> [Student]:
        pass

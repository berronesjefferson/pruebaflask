# from apps.tipti.infrasctructure.repository.interface.StudentRepository import StudentRepository
from apps.tipti.domain.model.student import Student
from apps.tipti.infrasctructure.database.entity.student_entity import StudentEntity
from apps.tipti.infrasctructure.model.student_model import StudentModel
from apps.tipti.infrasctructure.repository.student_repository import StudentRepository


class StudentRepositoryImplFile(StudentRepository):
    def get_list(self) -> [Student]:
        # leer archivo y obtener datos
        return [
            StudentModel(
                first_name="lee al archivo",
                last_name="crea Studen model",
                email="devu",
                age=1,
            ).to_domain()
        ]

    def get_student(self, id):
        pass

    def create(self, student_entity: StudentEntity) -> None:
        # 1 : lee el archivo
        # 2 : adiciona el registro
        # 2 : cierra el archivo
        pass

    def update(self, student_entity: StudentEntity):
        pass

    def delete(self, id):
        pass

    def get_list_from_api(self):
        pass

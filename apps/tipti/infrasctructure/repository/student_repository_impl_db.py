from apps.tipti.domain.model.student import Student
from apps.tipti.infrasctructure.database.entity.student_entity import StudentEntity
from apps.tipti.infrasctructure.network.student_service import StudentService
from apps.tipti.infrasctructure.repository.student_repository import StudentRepository


class StudentRepositoryImplDb(StudentRepository):

    def __init__(self, student_service: StudentService):
        self.student_service = student_service

    def get_list(self) -> [Student]:
        return [x.to_domain() for x in StudentEntity.get_all()]

    def get_student(self, id) -> Student:
        register_or_none = StudentEntity.get_by_id(id)
        if register_or_none is not None:
            return register_or_none.to_domain()
        raise Exception("Register not found")

    def create(self, student_entity: StudentEntity) -> None:
        student_entity.save()

    def update(self, student_entity: StudentEntity) -> None:
        student_entity.save()

    def delete(self, id):
        register_or_none = StudentEntity.get_by_id(id)
        if register_or_none is not None:
            register_or_none.delete()
        # Todo : Implement custom exception
        raise NameError("Registro no encontrado en la base de datos")

    def get_list_from_api(self) -> [Student]:
        return [x.to_domain() for x in self.student_service.get_list()]

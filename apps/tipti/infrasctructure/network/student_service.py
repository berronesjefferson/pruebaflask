from apps.tipti.infrasctructure.model.student_model import StudentModel


class StudentService:

    def __init__(self):
        pass

    def get_list(self) -> [StudentModel]:
        return [
            StudentModel(
                first_name="fiticio",
                last_name="fiticio",
                email="fiticio@gmail.com",
                age=1000,
            )
        ]

    def get_student(self) -> StudentModel:
        pass

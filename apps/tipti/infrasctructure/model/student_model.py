from apps.tipti.domain.model.student import Student


class StudentModel:
    __first_name: str
    __last_name: str
    __email: str
    __age: int

    def __init__(self, first_name, last_name, email, age):
        self.__first_name = first_name
        self.__last_name = last_name
        self.__email = email
        self.__age = age

    def to_domain(self):
        return Student(
            first_name=self.__first_name,
            last_name=self.__last_name,
            email=self.__email,
            age=self.__age,
        )

from core.config.db import db, BaseModelMixin


class StudentEntity(db.Model, BaseModelMixin):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String, nullable=False)
    last_name = db.Column(db.String, nullable=False)
    email = db.Column(db.String, unique=True, nullable=False)
    age = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return f'Student {self.id} {self.first_name} - {self.last_name} - {self.email} - {self.age}'

    # def to_JSON(self):
    #     return {
    #         'id': self.id,
    #         'first_name': self.first_name,
    #         'last_name': self.last_name,
    #         'email': self.email,
    #         'age': self.age,
    #     }

    def to_domain(self):
        from apps.tipti.domain.model.student import Student
        return Student(
            first_name=self.first_name,
            last_name=self.last_name,
            email=self.email,
            age=self.age,
        )

class Account:
    balance = 0

    def insert_money(self, amount):
        self.balance += amount

    def remove_money(self, amount):
        self.balance -= amount

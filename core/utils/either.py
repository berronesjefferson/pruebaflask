# from monad import Monad
import pymonad.tools

from pymonad.either import Left, Right

# a = Right(2)
# b = Left('Invalid')
#
# print(a.either(lambda x: f'Sorry, {x}', lambda x: x))  # 2
# print(b.either(lambda x: f'Sorry, {x}', lambda x: x))  # Sorry, Invalid

import os

from decouple import config
from flask import Flask
from core.config.db import db


class Config:
    SECRET_KEY = config('SECRET_KEY')


class DevelopmentConfig(Config):
    # SERVER_NAME = 'localhost:5000'
    DEBUG = True

    DATABASE_PATH = 'app/database/contact_book.db'
    # DB_TOKEN = os.environ.get('DB_TOKEN', '')  # Para encriptar la base de datos

    ENCRYPT_DB = True

    # TEMPLATE_FOLDER = 'views/templates/' -> No usar
    # STATIC_FOLDER = 'views/static/' -> No usar


class Testing(Config):
    TESTING = True


class Production(Config):
    pass


config = {
    'development': DevelopmentConfig,
    'testing': Testing,
    'production': Production,
    'default': DevelopmentConfig
}

import unittest

from apps.financial.account import Account
from parameterized import parameterized


# py -m unittest discover tests

class TestNewAccount(unittest.TestCase):

    # def test_sum(self):
    #     sum = calc.sum(3, 2)
    #     assert sum == 5

    def test_new_account(self) -> None:
        # CUANDO SE CREA UNA NUEVA CUENTA, EL SALDO DE ESTE ES IGUAL A 0
        account = Account()
        self.assertEqual(account.balance, 0)

    def test_when_insert_new_account_the_balance_increase(self) -> None:
        # CUANDO HACEMOS UN INGRESO, EL SALDO DE ESTA CUENTA PASA A SER EL VALOR DEL SALDO INCIAL MAS  EL DINERO INGRESADO
        # cuenta recien creada
        account = Account()
        account.insert_money(500)
        self.assertEqual(account.balance, 500)

    def test_when_exist_account_and_the_balance_increase(self) -> None:
        # CUANDO HACEMOS UN INGRESO, EL SALDO DE ESTA CUENTA PASA A SER EL VALOR DEL SALDO INCIAL MAS  EL DINERO INGRESADO
        # Caso cuando ya tenia saldo
        account = Account()
        # Preparar
        account.insert_money(500)
        # Actuar
        account.insert_money(1000)
        # Comprobar
        self.assertEqual(account.balance, 1500)

    def test_when_remove_money_the_balance_is_equal_to_balance_initial_less_the_money_insert(self) -> None:
        # CUANDO RETIRAMO EL DINERO, EL SALDO DE LA CUENTA PASA A SER EL VALOR DEL SALDO INCIAL MENOS EL DINERO INGRESADO
        account = Account()
        # Preparar
        account.insert_money(500)
        account.insert_money(1000)
        # Actuar
        account.remove_money(1000)
        # Comprobar
        self.assertEqual(account.balance, 500)

    # def test_when_remove_money_and_balance_initial_is_negative_exec_exeption(self) -> None:
    #     # SI INTENTAMOS RETIRAR MAS DINERO DEL QUE TENEMOS EN LA CUENTA, SALTARA UNA EXCEPCION
    #     account = Account()
    #     # Preparar
    #     account.insert_money(500)
    #     account.insert_money(1000)
    #     # Actuar
    #     account.remove_money(1501)
    #     # Comprobar
    #
    #     try:
    #         if account.balance < 0:
    #             raise ValueError()
    #             # self.assertEqual(account.balance, 0)
    #     except Exception as ex:
    #         pass


# requisitoçs
"""
    Todo:
        iterador retone una version de string del entero cuando no sea divisible para 3 o para 5
"""


def get_fizzbuzz_value(value: int) -> str:
    if type(value) is not int:
        raise TypeError("get_fizzbuzz_value did not receive integer input")
    if value % 3 == 0:
        return "Fizz"
    if value % 5 == 0:
        return "Buzz"
    return f"{value}"


class TestFizzBuzz(unittest.TestCase):

    @parameterized.expand([
        [1, "1"],
        [2, "2"],
        [4, "4"]
    ])
    def test_it_returns_string_version_of_integer_when_not_divisble_by_3_or_5(self, input_value, expected_result):
        result = get_fizzbuzz_value(input_value)
        self.assertEqual(expected_result, result)

    @parameterized.expand([
        [3],
        [6],
        [9]
    ])
    def test_it_returns_Fizz_when_given_multiples_of_3(self, input_value):
        result = get_fizzbuzz_value(input_value)
        self.assertEqual("Fizz", result)

    @parameterized.expand([
        [5],
        [10],
        [20],
    ])
    def test_it_returns_Buzz_when_given_multiples_of_5(self, input_value):
        result = get_fizzbuzz_value(input_value)
        self.assertEqual("Buzz", result)

    def test_it_raises_a_TypeError_when_not_given_integer_input(self):
        with self.assertRaisesRegex(TypeError, "get_fizzbuzz_value did not receive integer input"):
            get_fizzbuzz_value(None)

    def fizz_buzz(self):
        for x in range(0, 100):
            if x % 3 == 0 and x % 5 == 0:
                print("Fizz Buzz")
            else:
                if x % 3 == 0:
                    print("Fizz")
                else:
                    if x % 5 == 0:
                        print("Buzz")
                    else:
                        print(x)
            print("-")

    def test_new_account(self) -> None:
        # CUANDO SE CREA UNA NUEVA CUENTA, EL SALDO DE ESTE ES IGUAL A 0
        account = Account()
        self.assertEqual(account.balance, 0)


if __name__ == '__main__':
    unittest.main()
